#!/bin/bash
# cp launch_reminder.sh .git/hooks/pre-commit;chmod +x .git/hooks/pre-commit

rawurlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02x' "'$c"
     esac
     encoded+="${o}"
  done
  echo "${encoded}"    # You can either set a return variable (FASTER) 
  REPLY="${encoded}"   #+or echo the result (EASIER)... or both... :p
}

CALENDAR_URL_PREFIX='https://calendar.google.com/calendar/u/0/r/eventedit?dates='

IFS=$'\n';
for i in $(git --no-pager diff --cached | sed -n 's/^+#\(REVISION.*\)/\1/pg');
do 
   revision=$(rawurlencode "$i")
   calendar_url_suffix=$(echo "$revision" | sed -n 's/^\(REVISION.*\)\([0-9]\{8\}$\)/\2T080000\/\2T090000\&text=\1\&trp=true\&sf=true/pg');
   echo $CALENDAR_URL_PREFIX$calendar_url_suffix;
done
